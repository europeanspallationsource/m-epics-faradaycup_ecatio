require ecat2,2.1.2
require ecat2db,julenemalkorra
require iocStats,3.1.14

#epicsEnvSet("ARCH","linux-x86_64")
epicsEnvSet("TOP", "./")
epicsEnvSet("IOC", "faradaycup")
epicsEnvSet("PREF", "faradaycup")
epicsEnvSet("IOCST", "IocStat")
 
# Domain number, Frequency Hz, Auto Configuration, Auto Start
ecat2configure(0,1000,1,1)

#0  0:0   PREOP  +  EK1101 EtherCAT-Koppler (2A E-Bus, ID-Switch)              DB=yes
#1  0:1  INIT   E  EL3164 4K. Ana. Eingang 0-10V                              DB=yes
#2  0:2  PREOP  +  EL4104 4Ch. Ana. Ausgang 0-10V, 16bit                      DB=yes
#3  0:3  PREOP  +  EL2124 4K. Dig. Ausgang 5V, 20mA                           DB=yes


dbLoadRecords("ecat2ek1101.db", "PREFIX=${PREF}, MOD_ID=EK1101_Coupler, SLAVE_IDX=0")
dbLoadRecords("ecat2el3164.db", "PREFIX=${PREF}, MOD_ID=EL3164_AnalogInput, SLAVE_IDX=1")
dbLoadRecords("ecat2el4104.db", "PREFIX=${PREF}, MOD_ID=EL4104_AnalogOutput, SLAVE_IDX=2")
dbLoadRecords("ecat2el2124.db", "PREFIX=${PREF}, MOD_ID=EL2124_DigitalOutput, SLAVE_IDX=3")
dbLoadTemplate(iocAdminSoft.substitutions, "IOC=${PREF}:${IOCST}")


dbl > "${TOP}/${IOC}_PVs.list"



