require ecat2,2.1.2
require ecat2db,julenemalkorra
require iocStats,3.1.14

#epicsEnvSet("ARCH","linux-x86_64")
epicsEnvSet("TOP", "./")
epicsEnvSet("IOC", "faradaycup")
epicsEnvSet("PREF", "faradaycup")
epicsEnvSet("IOCST", "IocStat")
 
# Domain number, Frequency Hz, Auto Configuration, Auto Start
ecat2configure(0,1000,1,1)

#0  0:0   EK1100 EtherCAT Coupler             			     DB=yes
#1  0:1   EL1018 8-channel digital input terminal 24VDC, 10us        DB=yes
#2  0:2   EL2808 8-channel digital output 24VDC, 0.5A                DB=yes
#3  0:3   EL3004 4-channel analog input terminals -10...+10 V, single ended, 12 bit	DB=yes
#4  0:4   EL4004 4-channel analog output terminal 0...10V, 12 bit    DB=yes
#5  0:5   EL9505 Power supply terminal 5VDC	                     DB=yes
#6  0:6   EL2124 4-channel digital output terminal 5VDC              DB=yes


dbLoadRecords("ecat2ek1100.db", "PREFIX=${PREF}, MOD_ID=EK1100_Coupler, SLAVE_IDX=0")
dbLoadRecords("ecat2el1018.db", "PREFIX=${PREF}, MOD_ID=EL1018_DigitalInput, SLAVE_IDX=1")
dbLoadRecords("ecat2el2808.db", "PREFIX=${PREF}, MOD_ID=EL2808_DigitalOutput, SLAVE_IDX=2")
dbLoadRecords("ecat2el3004.db", "PREFIX=${PREF}, MOD_ID=EL3004_AnalogInput, SLAVE_IDX=3")
dbLoadRecords("ecat2el4004.db", "PREFIX=${PREF}, MOD_ID=EL4004_AnalogOutput, SLAVE_IDX=4")
dbLoadRecords("ecat2el9505.db", "PREFIX=${PREF}, MOD_ID=EL9505_PowerSupply_terminal, SLAVE_IDX=5)
dbLoadRecords("ecat2el2124.db", "PREFIX=${PREF}, MOD_ID=EL2124_DigitalOutput, SLAVE_IDX=6)
dbLoadTemplate(iocAdminSoft.substitutions, "IOC=${PREF}:${IOCST}")


dbl > "${TOP}/${IOC}_PVs.list"



